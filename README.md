# psycho-cred
[![Gitlab-CI](https://gitlab.com/SierraII/psycho-cred/badges/master/pipeline.svg)](https://gitlab.com/SierraII/psycho-cred/commits/master) [![License](https://img.shields.io/badge/License-MIT-blue.svg)]()

## Overview
### WTF is psycho-cred?
psycho-cred (or psycred for short) is a configuration evaluator written in Python.

### WTF do you mean?
Let's say you have a configuration file that contains sensitive information, and that file you don't want certain things to change from one thing to another accidentally. psycho-cred stops that from happening.

In a nutshell - it tests regression of configuration files.

### Rad! But How?
Inside psycho-cred are unique handlers. The first (and currently the only) handler is the docker-compose handler. You are able to register your `docker-compose.yml` file and environment variables that you don't want changed inside that `docker-compose.yml`.

This creates a `.psycred` file in the directory you specified that will contain all the provided information. This file can be run at anytime and will raise an error if there is any discrepancy between the original values provided and the configuration file.

### Ah cool! But Why?
I've been yelled at countless times for my poor development practice when accidentally committing AWS credentials in my `docker-compose.yml` files.

This will stop you from doing things like that.

Sorry Corne.


## Setup
psycho-cred is currently not available to download and install via `PyPI`. But we are able to set it up locally quite simply:

* Make sure you have `pip` installed. If you don't, you can install it [from here](https://pip.pypa.io/en/stable/installing/).
* Clone this repository:  
`git@gitlab.com:SierraII/psycho-cred.git`
* Go into the psycho-cred folder and run the following command to tell pip to install a local package:  
`pip install -e .`

## Usage
### CLI
`psycred [OPTIONS] COMMAND [ARGS]...`

* Show the menu of available commands and options:  
`psycred init --help`

* Initialize the `.psycred` file to be run:  
`psycred init -f [some-credential-file] -h [handler] -c [credential-key] [credential-value] -d [discriminator] [location]`

* Example:
`psycred init -f docker-compose.yml -c AWS_ACCESS_KEY_ID "" -h docker-compose -d api .`  

The output (and contents of the `.psycred` file) will be:
```yml
handlers:
- credentials:
  - AWS_ACCESS_KEY_ID: ''
  discriminators:
  - api
  handler: docker-compose
  location: docker-compose.yml
```

* Run the `.psycred` file:  
`psycred run .`

### Git Commit Hook
Ah! Would be cool that when you initialize a file, that the run command could run before you commit something to git and abort a commit if there are issues?

There is a `-g` flag for adding a pre-commit git hook to run psycred on the `.psycred` file. This only works if the folder you are initializing/running psycho-cred in is a git project (obviously).

## Handlers
### Discriminator Concept
Every handler will have a discriminator. Every handler is responsible for handling some configuration file type (docker-compose, Google App Engine, Kunernetes files).

A discriminator is a unique identifier (or group of) that a specific handler will use to compare configuration on.

### Docker-Compose
The docker-compose handler will iterate through services and check their environment variables to check for regression issues.

**This handler works for docker-compose files version 3**

Lets say you have a `docker-compose.yml` file that looks like this:
```yml
version: '3'
services:
  api_foo:
    image: 'node:6-alpine'
    environment:
     - NODE_ENV=local
     - AWS_SECRET_KEY=
  api_bar:
    image: 'node:6-alpine'
    environment:
     - NODE_ENV=production
     - AWS_SECRET_KEY=
```

The last thing we want to do is iterate through **all** services and check that the `NODE_ENV` hasn't changed to `foo`. Things will break.
The discriminators we then register are essentially the service names.

If you don't want the access key to change to anything else from a blank string in both services, you are able to add multiple discriminator arguments to the initialize function or the `.psycred` files.

`psycred init -f docker-compose.yml -c AWS_ACCESS_KEY_ID "" -h docker-compose -d api_foo -d api_bar .`

## Words of Warning
1) If you used the git pre-commit flag (`-g`) to add pre-commit steps and remove the `.psycred` file, things will break. You will need to remove the command from your pre-commit git hook file.  
2) You are able to register a credential file that doesn't exist. lol.  
3) Docker-compose files need to be version 3.  

## Contributions
**Lets work on this together!**  

Have an idea for a handler? Find a "small" bug or some improvements to make? Want to make some pipelines for linting and stuff? Want to write some tests?  
Feel free to submit a merge request :)
