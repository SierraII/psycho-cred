import click
from core.cli.run_cli import run
from core.cli.init_cli import init


@click.group()
def cli():
    pass


cli.add_command(run)
cli.add_command(init)
