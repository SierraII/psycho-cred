from core.environment import project_service
from click import command, argument, option, Path, Choice, Tuple


handler_choices = project_service.retrieve_handler_names()


@command()
@argument('location')
@option('--git-pre-commit-hook', '-g', is_flag=True,
        help='add pre-commit git hook')
@option('--handler', '-h', type=Choice(handler_choices), required=True,
        help='file handler type')
@option('--credential_file', '-f', required=True, multiple=True, type=Path(),
        help='credential file path')
@option('--discriminator', '-d', multiple=True,
        help='handler specific discriminator for structure selection')
@option('--credential', '-c', multiple=True, type=Tuple([str, str]),
        help='secure credential key with default check value')
def init(git_pre_commit_hook, credential_file, discriminator,
         credential, handler, location):
    project_service.initialize(
        location=location,
        handler_name=handler,
        credentials=credential,
        githook=git_pre_commit_hook,
        discriminators=discriminator,
        credential_files=credential_file,
    )
