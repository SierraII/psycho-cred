import click
from core.environment import project_service


@click.command()
@click.argument('location')
def run(location):
    project_service.run(location=location)
