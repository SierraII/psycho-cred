import os
import chalk
from core.cli import cli


def log(message):
    print(chalk.magenta('psycho-cred: ') + message)


try:
    cli()
except Exception as e:
    log(str(e))
    log('aborting with exit code 1')
    os._exit(1)
