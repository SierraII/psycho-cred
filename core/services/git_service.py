import os
import git
import stat
from core.errors import GitProjectNotFound

RUN_COMMAND = 'psycred run .'
GIT_PRE_COMMIT_PATH = '.git/hooks/pre-commit'


class GitService(object):
    def validate(self, location):
        try:
            git.Repo(location).git_dir
        except git.exc.InvalidGitRepositoryError:
            raise GitProjectNotFound(location=location)

    def create_pre_commit_hook(self, location):
        pre_commit_hook_path = f'{location}/{GIT_PRE_COMMIT_PATH}'
        self.create_pre_commit_file(pre_commit_hook_path)
        self.make_file_executable(pre_commit_hook_path)

    def create_pre_commit_file(self, location):
        with open(location, 'w+') as file:
            file.write(RUN_COMMAND)

    def make_file_executable(self, path):
        st = os.stat(path)
        os.chmod(path, st.st_mode | stat.S_IEXEC)
