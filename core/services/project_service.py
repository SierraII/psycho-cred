ROOT_ENTITY_NAME = 'handlers'


class ProjectService(object):
    def __init__(self, git_service, file_service, handler_service):
        self.git_service = git_service
        self.file_service = file_service
        self.handler_service = handler_service

    def initialize(self, credential_files, credentials, discriminators, handler_name, location, githook=False):  # NOQA
        if githook is True:
            self.git_service.validate(location)
            self.git_service.create_pre_commit_hook(location)

        entities = self.build_entity_file(
            credentials=credentials,
            handler_name=handler_name,
            discriminators=discriminators,
            credential_files=credential_files,
        )
        self.file_service.initialize_file(payload=entities, location=location)

    def build_entity_file(self, credential_files, discriminators, credentials, handler_name): # NOQA
        entities = {ROOT_ENTITY_NAME: []}
        for credential_file in credential_files:
            entry = self.handler_service.create_handler_entry(
                name=handler_name,
                credentials=credentials,
                location=credential_file,
                discriminators=discriminators,
            )
            entities[ROOT_ENTITY_NAME].append(entry)
        return entities

    def run(self, location):
        self.file_service.validate_project_file(location)
        self.handler_service.run(
            config=self.file_service.read_file(location),
            root_entity_name=ROOT_ENTITY_NAME,
        )

    def retrieve_handler_names(self):
        return self.handler_service.retrieve_handler_names()
