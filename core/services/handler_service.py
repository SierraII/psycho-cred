from core.errors import HandleNotFound

HANLDER_KEY_NAME = 'handler'
LOCATION_KEY_NAME = 'location'
DISCRIMINATOR_NAME = 'discriminators'
CREDENTIAL_VALUES_NAME = 'credentials'


class HandlerService(object):
    def __init__(self, handlers):
        self.handlers = handlers

    def create_handler_entry(self, credentials, location, name, discriminators): # NOQA
        return {
            HANLDER_KEY_NAME: name,
            LOCATION_KEY_NAME: location,
            DISCRIMINATOR_NAME: list(discriminators) if discriminators else None, # NOQA
            CREDENTIAL_VALUES_NAME: self.generate_payload(
                handler_name=name,
                credentials=credentials,
            )
        }

    def generate_payload(self, credentials, handler_name):
        handler = self.retrieve_handler(handler_name)
        return handler.generate_payload(credentials)

    def retrieve_handler(self, handler_name):
        for handler in self.handlers:
            if handler.name == handler_name:
                return handler
        raise HandleNotFound(handler_name)

    def run(self, config, root_entity_name):
        for entry in config[root_entity_name]:
            handler = self.retrieve_handler(entry[HANLDER_KEY_NAME])
            handler.handle(
                file_location=entry[LOCATION_KEY_NAME],
                discriminators=entry[DISCRIMINATOR_NAME],
                comparison_values=entry[CREDENTIAL_VALUES_NAME]
            )

    def retrieve_handler_names(self):
        names = []
        for handler in self.handlers:
            names.append(handler.name)
        return names
