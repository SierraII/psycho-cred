import yaml
from pathlib import Path
from core.errors import PsychoCredFileNotFound

NAME = 'psycred'
EXTENTION = 'yml'


class FileService(object):
    def initialize_file(self, payload, location):
        self.write_file(location=location, payload=payload)

    def write_file(self, location, payload):
        file_location = self.build_path(location)
        with open(file_location, 'w') as outfile:
            yaml.dump(payload, outfile, default_flow_style=False)

    def validate_project_file(self, location):
        file_path = self.build_path(location)
        path = Path(file_path)
        if not path.is_file():
            raise PsychoCredFileNotFound(location)

    def read_file(self, location):
        file_path = self.build_path(location)
        with open(file_path, 'r') as stream:
            return yaml.load(stream)

    def build_path(self, location):
        return f'{location}/.{NAME}.{EXTENTION}'
