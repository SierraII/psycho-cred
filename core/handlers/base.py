from abc import ABCMeta, abstractmethod


class Base(object, metaclass=ABCMeta):
    def __init__(self, name):
        self.name = name

    @abstractmethod
    def handle(self):
        pass

    @abstractmethod
    def compare(self):
        pass

    @abstractmethod
    def generate_payload(self):
        pass
