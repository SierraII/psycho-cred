import yaml
from pathlib import Path
from core.handlers.base import Base
from core.errors import (
    InvalidYamlError,
    FileNotFoundError,
    ValueMismatchError,
)


class DockerComposeHandler(Base):
    def __init__(self):
        self.name = 'docker-compose'

    def handle(self, file_location, discriminators, comparison_values):
        contents = self.read_file(file_location)
        services = discriminators if discriminators is not None else contents['services']  # NOQA
        for service_name in services:
            service = contents['services'][service_name]
            original, compare = self.build_values(
                compare=comparison_values,
                original=service['environment'],
            )
            self.compare(
                original=original,
                comparison=compare,
                discriminator=service_name,
            )

    def build_values(self, original, compare):
        original = self.parse_original_values(original)
        compare = self.parse_comparison_values(compare)
        return original, compare

    def parse_original_values(self, values):
        parsed_values = {}
        for value in values:
            parsed_key_value = value.split('=')
            key = parsed_key_value[0]
            value = parsed_key_value[1]
            parsed_values[key] = value
        return parsed_values

    def parse_comparison_values(self, values):
        memo = {}
        for obj in values:
            key = list(obj)[0]
            memo[key] = obj[key]
        return memo

    def compare(self, original, comparison, discriminator):
        for key, value in original.items():
            if key in comparison and value != comparison[key]:
                raise ValueMismatchError(
                    key=key,
                    found_value=value,
                    discriminator=discriminator,
                    expected_value=comparison[key],
                )

    def read_file(self, location):
        self.validate_file(location)
        try:
            with open(location, 'r') as stream:
                return yaml.load(stream)
        except Exception:
            raise InvalidYamlError(location)

    def validate_file(self, location):
        path = Path(location)
        if not path.is_file():
            raise FileNotFoundError(location)

    def strip_contents(contents):
        return contents['environment']

    def generate_payload(self, credentials):
        memo = []
        for credential in credentials:
            memo.append({credential[0]: credential[1]})
        return memo
