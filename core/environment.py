from core.services.git_service import GitService
from core.services.file_service import FileService
from core.services.handler_service import HandlerService
from core.services.project_service import ProjectService
from core.handlers.docker_compose import DockerComposeHandler

docker_compose_handler = DockerComposeHandler()
handler_service = HandlerService(
    handlers=[docker_compose_handler]
)

git_service = GitService()
file_service = FileService()
project_service = ProjectService(
    git_service=git_service,
    file_service=file_service,
    handler_service=handler_service,
)
