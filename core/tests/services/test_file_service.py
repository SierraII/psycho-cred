import os
import yaml
import shutil
from unittest import TestCase
from core.errors import PsychoCredFileNotFound
from core.services.file_service import FileService

_LOCAL_TEST_DIR = '_local_test'


class TestFileService(TestCase):
    def setUp(self):
        try:
            shutil.rmtree(_LOCAL_TEST_DIR)
        except FileNotFoundError:
            pass

        os.mkdir(_LOCAL_TEST_DIR)
        self.service = FileService()

    def tearDown(self):
        shutil.rmtree(_LOCAL_TEST_DIR)
        self.service = FileService()

    def test_build_path(self):
        expected_result = f'{_LOCAL_TEST_DIR}/.psycred.yml'
        result = self.service.build_path(_LOCAL_TEST_DIR)
        self.assertEqual(expected_result, result)

    def test_validate_project_file_valid(self):
        file_name = self.service.build_path(_LOCAL_TEST_DIR)
        with open(file_name, 'w') as outfile:
            yaml.dump({}, outfile, default_flow_style=False)

        self.service.validate_project_file(_LOCAL_TEST_DIR)

    def test_validate_project_file_invalid(self):
        with self.assertRaises(PsychoCredFileNotFound):
            self.service.validate_project_file('output')

    def test_read_file(self):
        expected_payload = {'foo': 'bar'}
        file_name = self.service.build_path(_LOCAL_TEST_DIR)
        with open(file_name, 'w') as outfile:
            yaml.dump(expected_payload, outfile, default_flow_style=False)

        payload = self.service.read_file(_LOCAL_TEST_DIR)
        self.assertEqual(expected_payload, payload)

    def test_initialize_file(self):
        payload = {'foo': 'bar'}
        file_name = self.service.build_path(_LOCAL_TEST_DIR)
        self.service.initialize_file(payload=payload, location=_LOCAL_TEST_DIR)
        self.assertTrue(os.path.isfile(file_name))

    def test_write_file(self):
        payload = {'foo': 'bar'}
        file_name = self.service.build_path(_LOCAL_TEST_DIR)
        self.service.initialize_file(payload=payload, location=_LOCAL_TEST_DIR)
        self.assertTrue(os.path.isfile(file_name))
