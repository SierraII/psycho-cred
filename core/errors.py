class FileNotFoundError(ValueError):
    def __init__(self, location):
        message = f'file {location} not found'
        super().__init__(message)


class HandleNotFound(ValueError):
    def __init__(self, handle_name):
        message = f'handle {handle_name} not found'
        super().__init__(message)


class InvalidYamlError(ValueError):
    def __init__(self, location):
        message = f'unable to parse yaml on {location}'
        super().__init__(message)


class GitProjectNotFound(ValueError):
    def __init__(self, location):
        message = f'{location} is not a git project'
        super().__init__(message)


class PsychoCredFileNotFound(ValueError):
    def __init__(self, location):
        message = f'psycho-cred file not found in {location}'
        super().__init__(message)


class ValueMismatchError(ValueError):
    def __init__(self, key, expected_value, found_value, discriminator):
        message = f'expected key {key} in {discriminator} to contain value "{expected_value}" but found "{found_value}"'  # NOQA
        super().__init__(message)
