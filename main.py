import os
import chalk
from core.cli import cli


if __name__ == '__main__':
    try:
        cli()
    except Exception as e:
        print(chalk.red('psycho-cred error: ') + e)
        os._exit(1)
