from setuptools import setup, find_packages

setup(
    name='psycho-cred',
    version='0.1',
    py_modules=find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
        'pyyaml',
        'GitPython',
        'pychalk',
    ],
    entry_points='''
        [console_scripts]
        psycho-cred=core.start
        psycred=core.start
    ''',
)
